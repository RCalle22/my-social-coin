// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.
importScripts("https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.2.1/firebase-messaging.js");

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
  apiKey: "AIzaSyA3dfD9hMsIkshMJo_EDL0htdsKHnAnu3o",
  authDomain: "my-social-coin.firebaseapp.com",
  projectId: "my-social-coin",
  storageBucket: "my-social-coin.appspot.com",
  messagingSenderId: "443935276247",
  appId: "1:443935276247:web:45be38cbafd2a286e98492",
  measurementId: "G-ZT3P4TR5XB"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
console.log("v3");
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
  console.log(
    "[firebase-messaging-sw.js] Received background message ",
    payload
  );
  // Customize notification here
  const notificationTitle =
    "Has recibido " + payload.data.coins + " social coins";
  const notificationOptions = {
    body: "Usuario: " + payload.data.uid,
    icon: "logo.png"
  };

  self.registration.showNotification(notificationTitle, notificationOptions);
});
console.log("register");
