import * as functions from "firebase-functions";
import admin = require("firebase-admin");
const firebase = admin.initializeApp();

exports.newUser = functions.auth.user().onCreate(async (user) => {
  await firebase
    .firestore()
    .collection("users")
    .doc(user.uid)
    .collection("transfersIn")
    .add({
      coins: 100,
      uid: "Nuevo usuario",
      date: new Date().toISOString()
    });
});

exports.newTransfer = functions.firestore
  .document("users/{uid}/transfersOut/{tid}")
  .onCreate(async (snap, context) => {
    const fromUId = context.params.uid;
    const data = snap.data();
    const transfer = {
      coins: Math.abs(data.coins),
      uid: fromUId,
      date: new Date().toISOString()
    };
    await firebase
      .firestore()
      .collection("users")
      .doc(data.uid)
      .collection("transfersIn")
      .add(transfer);
    const refPushTokens = await admin
      .firestore()
      .collection("users")
      .doc(data.uid)
      .collection("pushTokens")
      .get();
    const pushTokens = refPushTokens.docs.map((doc) => doc.id);
    functions.logger.log(pushTokens);
    await admin.messaging().sendMulticast({
      data: { coins: transfer.coins.toString(), uid: transfer.uid },
      tokens: pushTokens
    });
  });
