import Vue from "vue";

import "./styles/quasar.sass";
import lang from "quasar/lang/es.js";
import "@quasar/extras/material-icons/material-icons.css";
import { Quasar, Dialog, Loading, Notify } from "quasar";

Vue.use(Quasar, {
  config: {},
  plugins: {
    Dialog,
    Loading,
    Notify
  },
  lang: lang
});
