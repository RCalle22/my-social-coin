import Vue from "vue";
import Vuex from "vuex";
import { VuexModule, Module, Mutation, Action } from "vuex-class-modules";
import { Transfers } from "@/models";
import { Loading } from "@/services/decoration";
import {
  login,
  userOnChange,
  getTransfers,
  makeTransfer,
  logout,
  extractCoins,
  enableNotifications,
  removeNotifications,
  getPushToken,
  messaging,
  onPushReceived
} from "@/services/firebase";
import { Notify } from "quasar";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {}
});

@Module
class StoreModule extends VuexModule {
  // state
  uid = "";
  coins = 0;
  transfersIn: Transfers[] = [];
  transfersOut: Transfers[] = [];
  pushToken = "";

  // getters
  get isLogged() {
    return this.uid !== "";
  }

  get transfers() {
    return [...this.transfersIn, ...this.transfersOut].sort(
      (a, b) => new Date(b.date).getTime() - new Date(a.date).getTime()
    );
  }
  get isUserActive() {
    return this.transfersIn.length > 1;
  }
  get isEnabledNotification() {
    return this.pushToken !== "" && Notification.permission === "granted";
  }
  @Mutation
  setUId(uid: string) {
    this.uid = uid;
  }
  @Mutation
  setTransfersIn(transfersIn: Transfers[]) {
    this.transfersIn = [...transfersIn];
  }
  @Mutation
  addNewTransfersOut(transfersOut: Transfers) {
    this.transfersOut = [...this.transfersOut, transfersOut];
  }
  @Mutation
  setTransfersOut(transfersOut: Transfers[]) {
    this.transfersOut = [...transfersOut];
  }
  @Mutation
  setMyCoins(coins: number) {
    this.coins = coins;
  }
  @Mutation
  updatePushToken(value: string) {
    this.pushToken = value;
  }

  @Action
  onUserChange() {
    userOnChange(this.setUId);
  }
  // actions
  @Action
  @Loading("Comprobando creadenciales", null, "Credenciales incorrectas")
  async loggin({
    newUser,
    email,
    pass
  }: {
    newUser: boolean;
    email: string;
    pass: string;
  }) {
    await login({ newUser, email, pass });
  }
  @Action
  @Loading("Cerrando session", null, "No pudimos cerrar sesión")
  async loggout() {
    await logout();
  }
  @Action
  @Loading(
    "Obteniendo datos de usuario",
    null,
    "Ningun dato de usuario encontrado"
  )
  async getUserData({ withPushToken }: { withPushToken: boolean }) {
    const transfers = await getTransfers(this.uid);
    this.setMyCoins(transfers.coins);
    this.setTransfersIn(transfers.transfersIn);
    this.setTransfersOut(transfers.transfersOut);
    if (!withPushToken) return;
    const pushTokens = await getPushToken(this.uid);
    const token = pushTokens.find(
      (token) => token === localStorage.getItem("pushtoken")
    );
    this.updatePushToken(token || "");
  }
  @Action
  @Loading(
    "Realizando transferencia",
    "Transferencia realizada",
    "Actualiza tu monedero. No podemos realizar la transferencia"
  )
  async makeTransfer({ to, coins }: { to: string; coins: number }) {
    const lastTransfer = this.transfersOut[this.transfersOut.length - 1];
    const transfer = await makeTransfer(
      this.uid,
      to,
      coins,
      lastTransfer ? lastTransfer.id : "0"
    );
    this.addNewTransfersOut(transfer);
    this.setMyCoins(extractCoins(this.transfersIn, this.transfersOut));
  }
  @Action
  onMessageReceived() {
    onPushReceived((notif) => {
      Notify.create({
        message: `Has recibido ${notif.coins} social coins. Actualiza`,
        color: "positive"
      });
    });
  }
  @Action
  @Loading(
    "Activando notificaciones",
    "Notificaciones activas",
    "No podemos activar las notificaciones"
  )
  async enableNotification(value: boolean) {
    if (value) {
      const pushToken = await enableNotifications(this.uid);
      this.updatePushToken(pushToken);
    } else {
      await removeNotifications(this.uid);
      this.updatePushToken("");
    }
  }
}

// register module (could be in any file)
export const storeModule = new StoreModule({ store, name: "store" });
