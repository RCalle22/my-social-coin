import firebase from "firebase";
export interface User {
  id: string;
  coins: number;
  active: boolean;
  transfers: Transfers[];
}

export interface Transfers {
  id: string;
  uid: string;
  coins: number;
  date: string;
}
