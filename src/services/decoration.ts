import { Loading as LQuasar, Notify } from "quasar";

export function Loading(
  loadingMessage: string | null = null,
  toastOk: string | null = null,
  toastError: string | null = null
) {
  return function(
    target: any,
    propertyName: string,
    propertyDesciptor: PropertyDescriptor
  ) {
    const method = propertyDesciptor.value;
    propertyDesciptor.value = async function(...args: any[]) {
      // convert list of greet arguments to string
      try {
        if (loadingMessage) LQuasar.show({ message: loadingMessage });
        const result = await method.apply(this, args);
        if (toastOk) Notify.create({ message: toastOk, color: "positive" });
        return result;
      } catch (e) {
        if (toastError) {
          console.log(e);
          Notify.create({ message: toastError, color: "negative" });
        }
      } finally {
        if (loadingMessage) LQuasar.hide();
      }
    };
    return propertyDesciptor;
  };
}
