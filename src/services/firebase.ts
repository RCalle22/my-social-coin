import firebase from "firebase";
import { Loading } from "quasar";
import { Transfers } from "../models/index";

// firebase init - add your own config here
const firebaseConfig = {
  apiKey: "AIzaSyA3dfD9hMsIkshMJo_EDL0htdsKHnAnu3o",
  authDomain: "my-social-coin.firebaseapp.com",
  projectId: "my-social-coin",
  storageBucket: "my-social-coin.appspot.com",
  messagingSenderId: "443935276247",
  appId: "1:443935276247:web:45be38cbafd2a286e98492",
  measurementId: "G-ZT3P4TR5XB"
};
const firebaseApp = firebase.initializeApp(firebaseConfig);
firebaseApp.analytics();
export const messaging = firebaseApp.messaging();

const userDB = firebaseApp.firestore().collection("users");
export async function login(form: {
  newUser: boolean;
  email: string;
  pass: string;
}) {
  form.newUser
    ? await firebaseApp
        .auth()
        .createUserWithEmailAndPassword(form.email, form.pass)
    : await firebaseApp
        .auth()
        .signInWithEmailAndPassword(form.email, form.pass);
}
export async function logout() {
  await firebaseApp.auth().signOut();
}

export function userOnChange(callback: (uid: string) => void) {
  Loading.show({ message: "Verificando credenciales" });
  firebaseApp.auth().onAuthStateChanged((user) => {
    if (!user) callback("");
    else callback(user?.uid);
    Loading.hide();
  });
}
export function extractCoins(
  transfersIn: Transfers[],
  transfersOut: Transfers[]
) {
  let myCoins = 0;
  myCoins = transfersIn.reduce((acc, value) => value.coins + acc, 0);
  myCoins = transfersOut.reduce((acc, value) => value.coins + acc, myCoins);
  return myCoins;
}
export async function getTransfers(uid: string) {
  const result = await Promise.all([
    userDB
      .doc(uid)
      .collection("transfersIn")
      .get(),
    userDB
      .doc(uid)
      .collection("transfersOut")
      .get()
  ]);
  const [transfersIn, transfersOut] = result.map((ref) =>
    ref.docs.map(
      (transfer) => ({ ...transfer.data(), id: transfer.id } as Transfers)
    )
  );
  return {
    transfersIn,
    transfersOut,
    coins: extractCoins(transfersIn, transfersOut)
  };
}
export async function getPushToken(uid: string) {
  const result = await userDB
    .doc(uid)
    .collection("pushTokens")
    .get();
  const tokens = result.docs.map((doc) => doc.id);
  if (!result) return [];
  return tokens;
}
export async function enableNotifications(uid: string) {
  const token = await messaging.getToken({
    vapidKey:
      "BJ42Gq1Iq7TqCQl0AjzGNfSsf0LBkf1fJrr_0nR93IQO3zkasB5eAUL1X36e_oKYAU7kfuFGOi5RfqL7JI11x-A"
  });
  await userDB
    .doc(uid)
    .collection("pushTokens")
    .doc(token)
    .set({});
  localStorage.setItem("pushtoken", token);
  return token;
}
export function onPushReceived(
  callback: (data: { coins: number; uid: string }) => void
) {
  messaging.onMessage((payload) => callback(payload.data));
}
export async function removeNotifications(uid: string) {
  await userDB.doc(uid).set({ pushToken: null });
  localStorage.removeItem("pushtoken");
}
export async function makeTransfer(
  uid: string,
  to: string,
  coins: number,
  transferBefore: string
) {
  const transfer: Omit<Transfers, "id"> = {
    coins: -coins,
    uid: to,
    date: new Date().toISOString()
  };
  const newId = (parseInt(transferBefore) + 1).toString();
  await userDB
    .doc(uid)
    .collection("transfersOut")
    .doc(newId)
    .set(transfer, { merge: false });
  return { ...transfer, id: newId };
}
